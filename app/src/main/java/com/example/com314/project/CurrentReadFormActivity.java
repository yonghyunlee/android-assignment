package com.example.com314.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TimePicker;

public class CurrentReadFormActivity extends AppCompatActivity {
    private Button bookDropdown;
    private Button registerBtn;
    private Button cancelBtn;
    private DatePicker loanStart;
    private DatePicker loanEnd;
    private EditText goalDayWeek;
    private TimePicker goalDayStart;
    private TimePicker goalDayEnd;
    private EditText goalPage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_form);

        // 각 컴포넌트를 가져옴
        bookDropdown = (Button) findViewById(R.id.bookDropdown);
        loanStart = (DatePicker) findViewById(R.id.loan_start);
        loanEnd = (DatePicker) findViewById(R.id.loan_end);
        goalDayWeek = (EditText) findViewById(R.id.goalDay_week);
        goalDayStart = (TimePicker) findViewById(R.id.goalDay_start);
        goalDayEnd = (TimePicker) findViewById(R.id.goalDay_end);
        goalPage = (EditText) findViewById(R.id.goalPageForm);
        registerBtn = (Button) findViewById(R.id.register_btn);
        cancelBtn = (Button) findViewById(R.id.cancel_btn);

        // 책 선택 버튼 클릭시 책 리스트 메뉴 표시
        bookDropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(CurrentReadFormActivity.this, v);

                for (String book : Data.historyTitles){
                    menu.getMenu().add(book);
                }

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) { //클릭시 버튼에 설정
                        bookDropdown.setText(item.getTitle());
                        return true;
                    }
                });
                menu.show();
            }
        });


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // 등록 버튼 클릭시

                // 대출 기간, 목표 시간 데이터 추출
                String loanDayString = String.valueOf(loanStart.getYear())+"."+
                        String.valueOf(loanStart.getMonth())+"."+
                        String.valueOf(loanStart.getDayOfMonth())+" ~ " +
                        String.valueOf(loanEnd.getYear())+"."+
                        String.valueOf(loanEnd.getMonth())+"."+
                        String.valueOf(loanEnd.getDayOfMonth());
                String goalDayString = goalDayWeek.getText().toString()+"\n"+
                        String.valueOf(goalDayStart.getCurrentHour()) +":"+
                        String.valueOf(goalDayStart.getCurrentMinute()) + " ~ " +
                        String.valueOf(goalDayEnd.getCurrentHour()) +":"+
                        String.valueOf(goalDayEnd.getCurrentMinute());


                // CurrentReadActivity 에서 받을 인텐트 생성
                Intent returnIntent = new Intent();

                // 인텐트 객체 안에 데이터 삽입
                returnIntent.putExtra("book", bookDropdown.getText().toString());
                returnIntent.putExtra("loanDay", loanDayString);
                returnIntent.putExtra("goalDay", goalDayString);
                returnIntent.putExtra("goalPage", String.valueOf(goalPage.getText()));

                // Result값을 RESULT_OK로 설정
                setResult(Activity.RESULT_OK,returnIntent);

                finish(); // 액티비티 종료

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // 취소 버튼 클릭시
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent); // RESULT_CANCELED 값을 인텐트에 설정
                finish(); // 종료
            }
        });
    }
}
