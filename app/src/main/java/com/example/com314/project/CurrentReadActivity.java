package com.example.com314.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CurrentReadActivity extends AppCompatActivity {
    private ImageView bookImage;
    private TextView bookTitle;
    private TextView bookLoanDay;
    private TextView bookGoalDay;
    private TextView bookGoalPage;
    private Button modifyPageBtn;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) { // CurrentReadFormActivity에서 값을 받아옴.
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){ // CurrentReadFormActivity에서 등록 버튼을 눌렀을 때
                String book=data.getStringExtra("book"); // 책 이름 추출
                String loanDay=data.getStringExtra("loanDay"); // 대출 기간 추출
                String goalDay=data.getStringExtra("goalDay"); // 목표 날짜 추출
                String goalPage=data.getStringExtra("goalPage"); // 목표 페이지 추출

                // 데이터 저장
                Data.currentReadBook = Data.historyTitles.indexOf(book);
                Data.currentLoanDay = loanDay;
                Data.currentGoalDay = goalDay;
                Data.currentGoalPage = Integer.decode(goalPage);

                // UI에 반영
                setUI();
            }
            if (resultCode == Activity.RESULT_CANCELED) { // CurrentReadFormActivity에서 취소 버튼을 눌렀을 때
                //만약 반환값이 없을 경우의 코드를 여기에 작성하세요.
                return;
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current); // activity_current로 레이아웃 설정

        // 각 컴포넌트를 가져옴
        bookImage = (ImageView) findViewById(R.id.bookImage);
        bookTitle = (TextView) findViewById(R.id.bookName);
        bookLoanDay = (TextView) findViewById(R.id.loanDay);
        bookGoalDay = (TextView) findViewById(R.id.goalDay);
        bookGoalPage = (TextView) findViewById(R.id.goalPage);
        modifyPageBtn = (Button) findViewById(R.id.modify_page_btn);

        // 데이터를 화면에 반영
        setUI();

        // 수정 버튼 클릭시 CurrentReadFormActivity로 이동
        modifyPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CurrentReadActivity.this, CurrentReadFormActivity.class);
                startActivityForResult(intent, 1); // 수정 페이지에서 데이터를 받아옴.
            }
        });
    }

    void setUI() { // 데이터로 UI 설정
        bookImage.setImageResource(Data.historyImages.get(Data.currentReadBook));
        bookTitle.setText(Data.historyTitles.get(Data.currentReadBook));
        bookLoanDay.setText(Data.currentLoanDay);
        bookGoalDay.setText(Data.currentGoalDay);
        bookGoalPage.setText(Data.currentGoalPage + " page");
    }
}
