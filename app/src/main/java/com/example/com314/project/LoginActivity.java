package com.example.com314.project;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class LoginActivity extends AppCompatActivity {
    private EditText idEdit;
    private EditText pwEdit;
    private Button loginBtn;
    private TextView loginStatus;
    private ProgressBar pb;

    @Override
    protected void onStop() { // 앱이 멈췄을 때 프로그래스바와 로그인 상태 TextView를 지움.
        pb.setVisibility(View.GONE);
        loginStatus.setVisibility(View.GONE);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login); // acticity_login으로 레이아웃 설정

        // 각 컴포넌트를 가져옴
        idEdit = (EditText) findViewById(R.id.id_edit);
        pwEdit = (EditText) findViewById(R.id.pw_edit);
        loginBtn = (Button) findViewById(R.id.login_btn);
        pb = (ProgressBar) findViewById(R.id.loadingProgress);
        loginStatus = (TextView) findViewById(R.id.login_status_text);

        // 로그인 버튼 클릭시 키보드를 내리기 위해 앱기능 input 서비스를 가져옴.
        final InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() { @Override public void run() {
                    runOnUiThread(new Runnable() {
                            public void run() { // 메시지 큐에 저장될 메시지의 내용
                                assert inputMethodManager != null;
                                // 2개의 editText에서 키보드를 숨김
                                inputMethodManager.hideSoftInputFromWindow(idEdit.getWindowToken(),0);
                                inputMethodManager.hideSoftInputFromWindow(pwEdit.getWindowToken(),0);
                            }
                        });
                    }
                }).start();

                pb.setVisibility(View.VISIBLE); // 프로그래스바 생성

                //EditText에서 값 가져옴.
                String id = String.valueOf(idEdit.getText());
                String password = String.valueOf(pwEdit.getText());

                // id와 password가 맞다면
                if (id.equals(Data.User.id) && password.equals(Data.User.password)) {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() { // 2초 뒤에 실행하게 메소드
                            // 초기 데이터 생성
                            Data.historyTitles.add("그림으로 쉽게 배우는 안드로이드 프로그래밍");
                            Data.historyImages.add(R.drawable.android);
                            Data.historySimpleMemo.add("3학년에 공부한 책");
                            Data.historyDetailMemo.add("3학년에 공부한 책입니다. 이 책을 통해 안드로이드에 대해 많을 걸 배웠습니다.");
                            Data.historyTitles.add("해리포터");
                            Data.historyImages.add(R.drawable.harry);
                            Data.historySimpleMemo.add("해리포터");
                            Data.historyDetailMemo.add("재밌는책");
                            Data.currentReadBook = 0;
                            Data.currentLoanDay = "2018.12.04 ~ 2018.12.11";
                            Data.currentGoalDay = "월 화 수\n14:00 ~ 16:00";
                            Data.currentGoalPage = 200;

                            // MenuActivity로 이동
                            Intent intent=new Intent(LoginActivity.this, MenuActivity.class);
                            startActivity(intent);
                        }
                    }, 2000);
                    // 상태 텍스트 표시
                    loginStatus.setTextColor(Color.BLUE);
                    loginStatus.setText("Success");
                    loginStatus.setVisibility(View.VISIBLE);
                } else { // 로그인 실패
                    pb.setVisibility(View.GONE);
                    loginStatus.setTextColor(Color.RED);
                    loginStatus.setText("잘못된 입력");
                    loginStatus.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
