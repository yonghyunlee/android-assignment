package com.example.com314.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class HistoryDetailActivity extends AppCompatActivity {
    private ImageView bookImage;
    private TextView title;
    private TextView simpleMemo;
    private TextView detailMemo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historydetail);

        // 각 컴포넌트 가져옴.
        bookImage = (ImageView) findViewById(R.id.bookImage);
        title = (TextView) findViewById(R.id.title);
        simpleMemo = (TextView) findViewById(R.id.simple_memo);
        detailMemo = (TextView) findViewById(R.id.detail_memo);

        // 무슨 책의 상세 페이지인지를 알기 위해 HistoryListActivity에서 보낸 인텐트를 가져옴.
        Intent intent = getIntent();

        int index = intent.getExtras().getInt("bookIndex"); // bookIndex키의 데이터를 추출

        // 해당하는 인덱스의 데이터를 UI에 표시
        bookImage.setImageResource(Data.historyImages.get(index));
        title.setText(Data.historyTitles.get(index));
        simpleMemo.setText(Data.historySimpleMemo.get(index));
        detailMemo.setText(Data.historyDetailMemo.get(index));
    }
}
