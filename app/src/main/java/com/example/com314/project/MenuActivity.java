package com.example.com314.project;

import android.content.Intent;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MenuActivity extends AppCompatActivity {
    private ImageView profile;
    private Button currentPageBtn;
    private Button histroyPageBtn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu); // activity_menu로 레이아웃 설정

        profile = (ImageView) findViewById(R.id.profile);

        profile.setBackground(new ShapeDrawable(new OvalShape())); // 프로필 사진을 동그란 모양으로
        profile.setClipToOutline(true); // 테두리 설정

        currentPageBtn = (Button) findViewById(R.id.current_page_btn); // 내가 읽고 있는 책 버튼 클릭 시 CurrentReadActivity로 화면 이동
        currentPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, CurrentReadActivity.class);
                startActivity(intent);
            }
        });

        histroyPageBtn = (Button) findViewById(R.id.history_page_btn); // 내가 읽었던 책 버튼 클릭 시 HistoryListActivity 화면 이동
        histroyPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, HistoryListActivity.class);
                startActivity(intent);
            }
        });
    }
}
