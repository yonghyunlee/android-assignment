package com.example.com314.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class HistoryListActivity extends AppCompatActivity {
    private ListView listView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historylist);

        // 책 리스트를 위한 어댑터 생성
        CustomList adapter = new CustomList(HistoryListActivity.this);
        listView=(ListView) findViewById(R.id.book_list);
        listView.setAdapter(adapter); //어댑터를 리스트에 설정
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() { // 각 책 선택 시 책 상세 페이지로 이동 -> HistoryDetailActivity
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), HistoryDetailActivity.class);
                intent.putExtra("bookIndex", position); // 어떤 책을 클릭했는지를 HistoryDetailActivity에 보내 주기 위해 인텐트에 값을 넣음.
                startActivity(intent);
            }
        });
    }

    private class CustomList extends ArrayAdapter<String> { // 리스트를 위한 adapter 클래스 정의
        private final Activity context;
        public CustomList(Activity context) {
            super(context, R.layout.list_books, Data.historyTitles); //historyTitles 데이터로 리스트 설정
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) { // 리스트 각 아이템 뷰 정의
            // 보여지기 위해선 inflate를 해야함.
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.list_books, null, true); //각 아이템 레이아웃 설정

            // 각 컴포넌트 가져옴
            ImageView imageView = (ImageView) rowView.findViewById(R.id.bookImage);
            TextView title = (TextView) rowView.findViewById(R.id.title);
            TextView simpleMemo = (TextView) rowView.findViewById(R.id.simple_memo);

            // 내용 설정
            imageView.setImageResource(Data.historyImages.get(position));
            title.setText(Data.historyTitles.get(position));
            simpleMemo.setText(Data.historySimpleMemo.get(position));

            return rowView;
        }
    }
}
