package com.example.com314.project;

import java.util.ArrayList;

public class Data {
    static class User { // 사용자 데이터
        public final static String id = "20161776";
        public final static String password = "970102";
    }
    public final static ArrayList<String> historyTitles = new ArrayList<>(); // 내가 읽었던 책 제목
    public final static ArrayList<Integer> historyImages = new ArrayList<>(); // 내가 읽었던 책 이미지
    public final static ArrayList<String> historySimpleMemo = new ArrayList<>(); // 내가 읽었던 책 간단 메모
    public final static ArrayList<String> historyDetailMemo = new ArrayList<>(); // 내가 읽었던 책 자세한 메모
    public static Integer currentReadBook; //내가 읽고 있는 책 인덱스
    public static String currentLoanDay; //내가 읽고 있는 책 대출 기간
    public static String currentGoalDay; //내가 읽고 있는 책 목표 시간
    public static Integer currentGoalPage; //내가 읽고 있는 책 목표 페이지량
}
